# -*- coding: utf-8 -*-
import sys
sys.path.append('../lib/')
from  fileManager import *
import numpy
import collections

#--------------- Parameters to configure --------------------
featuresSourceFile = "../dataset/featureNames.txt"
datasetSourceFile = "../dataset/covtype.data"
outputDataFolder = outputDataFolder
outputContinuousReport = outputDataFolder + "DQR-ContinuousFeatures.csv"
outputCategoricalReport = outputDataFolder + "DQR-CategoricalFeatures.csv"
continuousFieldnames = ["Elevation", "Aspect", "Slope", "Horizontal_Distance_To_Hydrology", "Vertical_Distance_To_Hydrology", "Horizontal_Distance_To_Roadways", "Hillshade_9am", "Hillshade_Noon", "Hillshade_3pm", "Horizontal_Distance_To_Fire_Points"]
categoricalFieldnames = ["Wilderness_area_1", "Wilderness_area_2", "Wilderness_area_3", "Wilderness_area_4", "Soil_type_2702", "Soil_type_2703", "Soil_type_2704", "Soil_type_2705", "Soil_type_2706", "Soil_type_2717", "Soil_type_3501", "Soil_type_3502", "Soil_type_4201", "Soil_type_4703", "Soil_type_4704", "Soil_type_4744", "Soil_type_4758", "Soil_type_5101", "Soil_type_5151", "Soil_type_6101", "Soil_type_6102", "Soil_type_6731", "Soil_type_7101", "Soil_type_7102", "Soil_type_7103", "Soil_type_7201", "Soil_type_7202", "Soil_type_7700", "Soil_type_7701", "Soil_type_7702", "Soil_type_7709", "Soil_type_7710", "Soil_type_7745", "Soil_type_7746", "Soil_type_7755", "Soil_type_7756", "Soil_type_7757", "Soil_type_7790", "Soil_type_8703", "Soil_type_8707", "Soil_type_8708", "Soil_type_8771", "Soil_type_8772", "Soil_type_8776", "Cover_type"]
fieldnames = ["Elevation", "Aspect", "Slope", "Horizontal_Distance_To_Hydrology", "Vertical_Distance_To_Hydrology", "Horizontal_Distance_To_Roadways", "Hillshade_9am", "Hillshade_Noon", "Hillshade_3pm", "Horizontal_Distance_To_Fire_Points", "Wilderness_area_1", "Wilderness_area_2", "Wilderness_area_3", "Wilderness_area_4", "Soil_type_2702", "Soil_type_2703", "Soil_type_2704", "Soil_type_2705", "Soil_type_2706", "Soil_type_2717", "Soil_type_3501", "Soil_type_3502", "Soil_type_4201", "Soil_type_4703", "Soil_type_4704", "Soil_type_4744", "Soil_type_4758", "Soil_type_5101", "Soil_type_5151", "Soil_type_6101", "Soil_type_6102", "Soil_type_6731", "Soil_type_7101", "Soil_type_7102", "Soil_type_7103", "Soil_type_7201", "Soil_type_7202", "Soil_type_7700", "Soil_type_7701", "Soil_type_7702", "Soil_type_7709", "Soil_type_7710", "Soil_type_7745", "Soil_type_7746", "Soil_type_7755", "Soil_type_7756", "Soil_type_7757", "Soil_type_7790", "Soil_type_8703", "Soil_type_8707", "Soil_type_8708", "Soil_type_8771", "Soil_type_8772", "Soil_type_8776", "Cover_type"]
#------------------------------------------------------------

# read data from source csv file
# also cast integer stored as string into real integer
# output type is a dict of list and the number of elements (rows)
def extractData(filename,features):
    datas = dict()
    i = 0
    with open(filename, 'rU') as csvfile:
        data = csv.DictReader(csvfile, delimiter=',', fieldnames=features)
        for row in data:
            for key, value in row.iteritems():
                if value.strip() != '?'.strip() :
                    if unicode(value,'utf-8').isnumeric():
                        value = int(value)
                    if key in datas:
                        datas[key].append(value)
                    else:
                        datas[key]= [value]
            i += 1
        return [datas, i]


# Calculate mean, max, ... for continuous and categorical
def processData(extract, i):
    continuous = dict()
    categorical = dict()
    for data in extract:
        if data in categoricalFieldnames: #categorical
           categorical[data] = [i]
           categorical[data].append((float(i-len(extract[data]))/i)*100)#miss
           categorical[data].append(cardinality(extract[data]))#card
           mode = calculateMode(extract[data])#mode + 2nd mode
           #first mode
           categorical[data].append(mode[0][0])#mode
           categorical[data].append(mode[0][1])#mode freq
           categorical[data].append(float(mode[0][1])/len(extract[data])*100)#mode pourcentage
           #2nd mode
           categorical[data].append(mode[1][0])#mode
           categorical[data].append(mode[1][1])#mode freq
           categorical[data].append(float(mode[1][1])/len(extract[data])*100)#mode pourcentage
        else:
            if data in continuousFieldnames: #continuous
                extract[data] = map(float,extract[data])
                continuous[data] = [i] #count
                continuous[data].append((float(i-len(extract[data]))/i)*100)#miss
                continuous[data].append(cardinality(extract[data]))#card
                continuous[data].append(calculateMinimum(extract[data]))#min
                continuous[data].append(calculateFirstQuarter(extract[data]))#first Quarter
                continuous[data].append(calculateMean(extract[data]))#mean
                continuous[data].append(calculateMedian(extract[data]))#median
                continuous[data].append(calculateThirdQuarter(extract[data]))#third Quarter
                continuous[data].append(calculateMaximum(extract[data]))#maximum
                continuous[data].append(calculateStd(extract[data]))#stdDEV

    return [categorical, continuous]

# Calculation functions

def calculateMaximum(values):
    return numpy.max(values)

def calculateMinimum(values):
    return numpy.min(values)

def calculateMean(values):
    return numpy.mean(values)

def calculateMedian(values):
    return numpy.median(values)

def calculateFirstQuarter(values):
    return numpy.percentile(values, 25)

def calculateThirdQuarter(values):
    return numpy.percentile(values, 75)

def cardinality(values):
    cnt = collections.Counter()
    for nb in values:
        cnt[nb] += 1
    return len(cnt)

def calculateStd(data):
    return numpy.std(data)

def calculateMode(data):
    cnt = collections.Counter()
    for nb in data:
        cnt[nb] += 1
    cnt=cnt.most_common(2)
    return cnt

# get date from a file or filenames constant defined on top
# 1 = from file, 0 = from filenames constant
def getFieldNames(mode):
  if(mode == 1):
    return readTextFile(featuresSourceFile);
  else:
    return fieldnames

def main():
    # read source, extract its data and do calculations
    fieldnames = getFieldNames(0)
    extract = extractData(datasetSourceFile, fieldnames)
    process = processData(extract[0], extract[1])

    # Create output Data directory if not exists
    createFolder(outputDataFolder)

    # Columns of the results tables for continuous and categorical
    continuousReportFieldnames = ["FeatureName", "Count", "Miss", "Card", "Min", "Qrt1", "Mean", "Median", "Qrt3", "Max", "Dev"]
    categoricalReportFieldnames = ["FeatureName","Count", "Miss", "Card", "Mode", "Mode Freq", "Mode %", "2nd Mode", "2nd Mode freq", "2nd Mode %"]

    # Output results into separated csv files
    writeCSVFile(process[1],continuousReportFieldnames, outputContinuousReport)
    writeCSVFile(process[0],categoricalReportFieldnames, outputCategoricalReport)

main()
