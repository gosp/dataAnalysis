# Tony Hennequin & Kevin Boeuf

import pandas as pd
from pandas import DataFrame
from sklearn import cross_validation
from sklearn import preprocessing
from sklearn import datasets
from sklearn.feature_extraction import DictVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt

#--------------------------------------------
# Global configurations
#--------------------------------------------
datasetPath = "../dataset/covtype.data"
# List of descriptives features
descriptiveFeatures = ["Elevation", "Aspect", "Slope", "Horizontal_Distance_To_Hydrology", "Vertical_Distance_To_Hydrology", "Horizontal_Distance_To_Roadways", "Hillshade_9am", "Hillshade_Noon", "Hillshade_3pm", "Horizontal_Distance_To_Fire_Points", "Wilderness_area_1", "Wilderness_area_2", "Wilderness_area_3", "Wilderness_area_4", "Soil_type_2702", "Soil_type_2703", "Soil_type_2704", "Soil_type_2705", "Soil_type_2706", "Soil_type_2717", "Soil_type_3501", "Soil_type_3502", "Soil_type_4201", "Soil_type_4703", "Soil_type_4704", "Soil_type_4744", "Soil_type_4758", "Soil_type_5101", "Soil_type_5151", "Soil_type_6101", "Soil_type_6102", "Soil_type_6731", "Soil_type_7101", "Soil_type_7102", "Soil_type_7103", "Soil_type_7201", "Soil_type_7202", "Soil_type_7700", "Soil_type_7701", "Soil_type_7702", "Soil_type_7709", "Soil_type_7710", "Soil_type_7745", "Soil_type_7746", "Soil_type_7755", "Soil_type_7756", "Soil_type_7757", "Soil_type_7790", "Soil_type_8703", "Soil_type_8707", "Soil_type_8708", "Soil_type_8771", "Soil_type_8772", "Soil_type_8776"]
targetFeatureName = "Cover_type"
datasetRowCount = 581012
algorithmCriteria='entropy'

def main():

  	#--------------------------------------------
  	# Creation of the tree model
  	#--------------------------------------------

	# Read the file containing the dataset
	inputFile = pd.read_csv(datasetPath,index_col=False,na_values=['?'],nrows=datasetRowCount)

	# Get target feature values
	targetFeature = inputFile[targetFeatureName]

	# Identify descriptive features and create the corresponding dataset
	descriptiveFeatures = ["Elevation", "Aspect", "Slope", "Horizontal_Distance_To_Hydrology", "Vertical_Distance_To_Hydrology", "Horizontal_Distance_To_Roadways", "Hillshade_9am", "Hillshade_Noon", "Hillshade_3pm", "Horizontal_Distance_To_Fire_Points", "Wilderness_area_1", "Wilderness_area_2", "Wilderness_area_3", "Wilderness_area_4", "Soil_type_2702", "Soil_type_2703", "Soil_type_2704", "Soil_type_2705", "Soil_type_2706", "Soil_type_2717", "Soil_type_3501", "Soil_type_3502", "Soil_type_4201", "Soil_type_4703", "Soil_type_4704", "Soil_type_4744", "Soil_type_4758", "Soil_type_5101", "Soil_type_5151", "Soil_type_6101", "Soil_type_6102", "Soil_type_6731", "Soil_type_7101", "Soil_type_7102", "Soil_type_7103", "Soil_type_7201", "Soil_type_7202", "Soil_type_7700", "Soil_type_7701", "Soil_type_7702", "Soil_type_7709", "Soil_type_7710", "Soil_type_7745", "Soil_type_7746", "Soil_type_7755", "Soil_type_7756", "Soil_type_7757", "Soil_type_7790", "Soil_type_8703", "Soil_type_8707", "Soil_type_8708", "Soil_type_8771", "Soil_type_8772", "Soil_type_8776"]
	dataset = inputFile[descriptiveFeatures]

	# Create a nearest neighbor model
	knn = KNeighborsClassifier()

	# Split the data: 50% training : 50% test set
	trainDataset, testDataset, trainTarget, testTarget = cross_validation.train_test_split(dataset, targetFeature, test_size=0.50, random_state=0)

	# Fit the model using the train dataset defined above
	knn.fit(trainDataset, trainTarget)

  	#--------------------------------------------
  	# Test Model
  	#--------------------------------------------

	# Use the model to make predictions for the test set queries
	predictions = knn.predict(testDataset)

	# Output the accuracy score of the model on the test set
	print("Accuracy= " + str(accuracy_score(testTarget, predictions, normalize=True)))

	# Calculate the confusion matrix according to the test set and predictions
	confusionMatrix = confusion_matrix(testTarget, predictions)
	print(confusionMatrix)
	print("\n\n")

	# Print confusion matrix
	plt.matshow(confusionMatrix)
	plt.title('Confusion matrix')
	plt.colorbar()
	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.show()

	#--------------------------------------------
	# Cross-validation to Compare to Models
	#--------------------------------------------

	# run a 5 fold cross validation on this model using the full census data
	scores=cross_validation.cross_val_score(knn, trainDataset, trainTarget, cv=5)
	# the cross validaton function returns an accuracy score for each fold
	print("Entropy based Model:")
	print("Score by fold: " + str(scores))
	# we can output the mean accuracy score and standard deviation as follows:
	print("Accuracy: %0.4f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
	print("\n\n")

main()
