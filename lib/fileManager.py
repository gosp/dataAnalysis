# -*- coding: utf-8 -*-
import csv
import os

# read source file
def readTextFile(filename):
    with open(filename, 'r') as f:
        listF = list()
        for line in f:
            listF.append(line.strip())
        return listF

# output data into a csv file
# <data> :
#   dict
#   (
#     ["featureName1"] = list(0, 1, 2, ...)
#     ...
#     ["featureName2"] = list(0, 1, 2, ...)
#   )
def writeCSVFile(data, fieldnames, filename):
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for featureName in data:
            tempDict = dict()
            i = 0
            for field in fieldnames:
                if field.strip() == fieldnames[0].strip():
                    tempDict[field] = featureName
                else:
                    tempDict[field] = data[featureName][i-1]
                i = i + 1
            writer.writerow(tempDict)

def createFolder(directoryPath):
  if not os.path.exists(directoryPath):
    os.makedirs(directoryPath)

version = '0.1'
