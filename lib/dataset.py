# -*- coding: utf-8 -*-
import csv
import os
import sys
import pandas

class Dataset:

  def __init__(self, datasetPath, features, numRows):
    self.target = dict()
    self.train = dict()
    self.validation = dict()
    self.test = dict()
    self.path = datasetPath
    self.numRows = numRows
    self.features = features

  def loadDataset(self):
    self.dataset = pandas.read_csv(self.path, names=self.features, index_col=False, na_values=['?'], nrows=self.numRows)

  def getTrainSet(self):
    return self.train

  def setTarget(self, target):
    self.target = target

  def setDatasetPath(self, path):
    self.path = path

  def setDatasetNumRows(self, numRows):
    self.numRows = numRows

  def setTrainProportion(self, proportion):
    self.targetProportion = proportion

  def displayDataset(self):
    print(self.dataset)

  def test2(self):
    a = self.dataset.head(2)
    trainProportion = 0.1
    validationProportion = 0.4
    testProportion = 0.2

    trainSetRowsCount = trainProportion * self.numRows
    validationSetRowsCount = validationProportion * self.numRows
    testSetRowsCount = testProportion * self.numRows

    # get training values set
    for (i = 0; i < trainSetRowsCount - 1; i++)
      self.train.push
    b = self.dataset.irow(0)
    print(b["Aspect"])

features = ["Elevation", "Aspect", "Slope", "Horizontal_Distance_To_Hydrology", "Vertical_Distance_To_Hydrology", "Horizontal_Distance_To_Roadways", "Hillshade_9am", "Hillshade_Noon", "Hillshade_3pm", "Horizontal_Distance_To_Fire_Points", "Wilderness_area_1", "Wilderness_area_2", "Wilderness_area_3", "Wilderness_area_4", "Soil_type_2702", "Soil_type_2703", "Soil_type_2704", "Soil_type_2705", "Soil_type_2706", "Soil_type_2717", "Soil_type_3501", "Soil_type_3502", "Soil_type_4201", "Soil_type_4703", "Soil_type_4704", "Soil_type_4744", "Soil_type_4758", "Soil_type_5101", "Soil_type_5151", "Soil_type_6101", "Soil_type_6102", "Soil_type_6731", "Soil_type_7101", "Soil_type_7102", "Soil_type_7103", "Soil_type_7201", "Soil_type_7202", "Soil_type_7700", "Soil_type_7701", "Soil_type_7702", "Soil_type_7709", "Soil_type_7710", "Soil_type_7745", "Soil_type_7746", "Soil_type_7755", "Soil_type_7756", "Soil_type_7757", "Soil_type_7790", "Soil_type_8703", "Soil_type_8707", "Soil_type_8708", "Soil_type_8771", "Soil_type_8772", "Soil_type_8776", "Cover_type"]
dataset = Dataset("../dataset/covtype.data", features, 581012)
dataset.loadDataset()
#dataset.displayDataset()
dataset.test2()
